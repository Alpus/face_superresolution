FROM pytorch/pytorch
LABEL maintainer="Alexander Pushin work@apushin.com"

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

EXPOSE 8888

ENTRYPOINT jupyter notebook --ip=0.0.0.0 --allow-root
